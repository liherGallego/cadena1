package cuatrovientos.ed.Cadena1;

public class Cadena {

	public Cadena() {
		
	}
	public int longitud(String cadena) {
		//Devuelve la longitud de la cadena que se le pasa.
		return cadena.length();
	}
	public int vocales(String cadena) {
		//Devuelve el número de vocales minúsculas que tiene la cadena
		int numeroMinusculas = 0;
		char[] vocales ={'a','e','i','o','u'};
		char[] cadenaArray = cadena.toCharArray();
		for (char c:cadenaArray) {
			for (char x:vocales) {
				if (c == x) {
					numeroMinusculas ++;
				}
			}
		}
		return numeroMinusculas;
	}
	public String invertir(String cadena) {
		String cadenaInversa = "" ;
		
		for (int x=cadena.length()-1;x>=0;x--) {
			cadenaInversa += cadena.charAt(x);
			}
		return cadenaInversa;
		}
		
	
	public int contarLetra(String cadena, char caracter) {
		//Cuenta el número de veces que aparece el carácter en la cadena.
		
		int numeroApariciones = 0;
		cadena = cadena.toLowerCase();
		char[] cadenaArray = cadena.toCharArray();
		for (char c:cadenaArray) {
		
				if (c == caracter) {
					numeroApariciones ++;
				}
			
		}
		return numeroApariciones;
	}
}

