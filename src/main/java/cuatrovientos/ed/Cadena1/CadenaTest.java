package cuatrovientos.ed.Cadena1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CadenaTest {
	private static Cadena cadena;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cadena = new Cadena();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		System.out.println("pruebas unitarias Cadena");
		
		System.out.println(cadena.invertir(""));
		System.out.println(cadena.invertir("papagayo"));
		
		System.out.println(cadena.longitud(""));
		System.out.println(cadena.longitud("australopithecus"));
		
		System.out.println(cadena.vocales(""));
		System.out.println(cadena.vocales("murcielago"));
		
		System.out.println(cadena.contarLetra("", ' '));
		System.out.println(cadena.contarLetra("Abracadabra", 'a'));
	}
	
}
